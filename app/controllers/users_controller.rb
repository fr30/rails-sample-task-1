class UsersController < ApplicationController
  before_action :authenticate_user!

  def index
    @users = User.all
  end

  def show
    @user = current_user
  end

  def nearby
    set_user_ip
    @users = current_user.nearbys(100000)
  end

end
