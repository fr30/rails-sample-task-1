class HobbiesController < ApplicationController
  before_action :authenticate_user!

  def index
    @hobbies = Hobby.all
    @user_hobby = UserHobby.new
  end

  def new
    @hobby = Hobby.new
    render 'new'
  end

  def create
    @hobby = Hobby.new(hobby_params)
    if @hobby.save
      redirect_to hobbies_url, notice: 'Hobby has been saved'
    else
      render 'new'
    end
  end

  private

  def hobby_params
    params.require(:hobby).permit(:name)
  end

end