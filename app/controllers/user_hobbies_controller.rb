class UserHobbiesController < ApplicationController
  before_action :authenticate_user!

  def new
    @user_hobby = UserHobby.new
    render 'new'
  end

  def create
    @user_hobby = UserHobby.new(user_hobby_params)
    if @user_hobby.save
      redirect_to hobbies_url, notice: 'Hobby has been assigned'
    else
      redirect_to hobbies_url
    end
  end

  private

  def user_hobby_params
    params.require(:user_hobby).permit(:user_id, :hobby_id)
  end
end
