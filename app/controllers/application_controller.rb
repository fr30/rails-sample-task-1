class ApplicationController < ActionController::Base
  before_action :configure_permitted_parameters, if: :devise_controller?
  protected

  def configure_permitted_parameters
    devise_parameter_sanitizer.permit(:sign_up, keys: [:username])
  end

  def user_ip
    request.remote_ip
  end

  def set_user_ip
    current_user.update(ip_address: user_ip)
  end
end
