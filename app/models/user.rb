class User < ApplicationRecord
  has_many :user_hobbies
  has_many :hobbies, through: :user_hobbies

  devise :database_authenticatable, :registerable,
         :rememberable, :validatable

  validates :username, presence: true

  geocoded_by :ip_address
  after_validation :geocode

end
