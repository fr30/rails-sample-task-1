class Hobby < ApplicationRecord
  has_many :user_hobbies
  has_many :users, through: :user_hobbies
  validates :name, presence: true, uniqueness: true
end
