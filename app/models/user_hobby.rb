class UserHobby < ApplicationRecord
  validates :hobby_id, :uniqueness => { :scope => [:user_id] }
  belongs_to :user
  belongs_to :hobby
end
