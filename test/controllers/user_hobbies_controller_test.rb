require 'test_helper'

class UserHobbiesControllerTest < ActionDispatch::IntegrationTest
  setup do
    @user_hobby = user_hobbies(:one)
  end

  test "should get index" do
    get user_hobbies_url
    assert_response :success
  end

  test "should get new" do
    get new_user_hobby_url
    assert_response :success
  end

  test "should create user_hobby" do
    assert_difference('UserHobby.count') do
      post user_hobbies_url, params: { user_hobby: {  } }
    end

    assert_redirected_to user_hobby_url(UserHobby.last)
  end

  test "should show user_hobby" do
    get user_hobby_url(@user_hobby)
    assert_response :success
  end

  test "should get edit" do
    get edit_user_hobby_url(@user_hobby)
    assert_response :success
  end

  test "should update user_hobby" do
    patch user_hobby_url(@user_hobby), params: { user_hobby: {  } }
    assert_redirected_to user_hobby_url(@user_hobby)
  end

  test "should destroy user_hobby" do
    assert_difference('UserHobby.count', -1) do
      delete user_hobby_url(@user_hobby)
    end

    assert_redirected_to user_hobbies_url
  end
end
