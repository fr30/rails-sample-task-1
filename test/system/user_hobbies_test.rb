require "application_system_test_case"

class UserHobbiesTest < ApplicationSystemTestCase
  setup do
    @user_hobby = user_hobbies(:one)
  end

  test "visiting the index" do
    visit user_hobbies_url
    assert_selector "h1", text: "User Hobbies"
  end

  test "creating a User hobby" do
    visit user_hobbies_url
    click_on "New User Hobby"

    click_on "Create User hobby"

    assert_text "User hobby was successfully created"
    click_on "Back"
  end

  test "updating a User hobby" do
    visit user_hobbies_url
    click_on "Edit", match: :first

    click_on "Update User hobby"

    assert_text "User hobby was successfully updated"
    click_on "Back"
  end

  test "destroying a User hobby" do
    visit user_hobbies_url
    page.accept_confirm do
      click_on "Destroy", match: :first
    end

    assert_text "User hobby was successfully destroyed"
  end
end
