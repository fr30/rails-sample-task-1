#TODO write tests for routes (check where you can get without authentication)
Rails.application.routes.draw do
  resources :users, only: [:index]
  resources :user_hobbies, only: [:index, :new, :create]
  devise_for :users, controllers: { sessions: 'users/sessions', registrations: 'users/registrations' }
  resources :hobbies, only: [:index, :new, :create]
  root to: 'users#show'
  get 'users/:id/nearby', to: 'users#nearby', as: 'users_nearby'
end
