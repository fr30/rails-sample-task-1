The app allows you to create users with simple forms. User model was implemented using gem devise. Every user can create and attach himself a hobby.

Also every user is geocoded using gem geocoder. There's an endpoint showing nearby users.

In order to start the app, you have to type standard commands for starting ruby server and generate a database, namely:

bundle install

rake db:setup

rails server

List of available endpoints can be obtained by:

rails routes

However, from to root path it's pretty intuitive how to move around the site.
