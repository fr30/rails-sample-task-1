# This file should contain all the record creation needed to seed the database with its default values.
# The data can then be loaded with the rails db:seed command (or created alongside the database with db:setup).
#
# Examples:
#
#   movies = Movie.create([{ name: 'Star Wars' }, { name: 'Lord of the Rings' }])
#   Character.create(name: 'Luke', movie: movies.first)

for i in (1..10) do
  User.create(email: Faker::Internet.unique.email,
              username:Faker::Name.unique.first_name,
              password: "123456",
              ip_address: Faker::Internet.unique.public_ip_v4_address)

end

for i in (1..10) do
  hobby = Hobby.create(
      name: Faker::Beer.unique.style)
  for j in (1..rand(1..3)) do
    UserHobby.create(user: User.where(id: rand(1...10)).first, hobby: hobby)
  end
end