class AddUserCoordinates < ActiveRecord::Migration[5.2]
  def change
    add_column :users, :latitude, :decimal
    add_column :users, :longitude, :decimal
    add_column :users, :ip_address, :string
  end
end
