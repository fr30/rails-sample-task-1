class CreateUserHobbies < ActiveRecord::Migration[5.2]
  def change
    create_table :user_hobbies do |t|
      t.timestamps
      t.integer :user_id
      t.integer :hobby_id
      t.index [:user_id, :hobby_id], unique: true
    end
  end
end
