class CreateHobbies < ActiveRecord::Migration[5.2]
  def change
    create_table :hobbies do |t|
      t.string :name, null: false, default: ""
      t.timestamps
      t.index :name, unique: true
    end
  end
end
